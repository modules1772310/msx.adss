# msx.adss

## Для чего нужен модуль
Модуль предначен для хранения настроек в форматах html, checkbox , file , photo, select , multiselect 

## Как работать с модулем 

В корне модуля есть файл ***adss.setting.php*** в котором можно задать нужные табы и поля  https://prnt.sc/XFEoKQanaoG2

Для получения значений свойст можно воспользоваться 2 функциями ***getOptionAdss($type,$code)*** и ***getOptionsAdssByBlock(blockName)***
