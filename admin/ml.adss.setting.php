<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
use Msx\Adss\Controllers\Tabs;
use Msx\Adss\Controllers\BaseController;
use Msx\Adss\Controllers\Options;
use \Bitrix\Main\Config\Option;
global $APPLICATION;
IncludeModuleLangFile(__FILE__);
$moduleID = "msx.adss";
\Bitrix\Main\Loader::includeModule($moduleID);

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
$context = \Bitrix\Main\Application::getInstance()->getContext();
$request = $context->getRequest();
$arPost = $request->getPostList()->toArray();
$arPost = $APPLICATION->ConvertCharsetArray($arPost, 'UTF-8', LANG_CHARSET);

BaseController::init();
Tabs::setTabs();

$tabControl = new CAdminTabControl("tabControl", Tabs::getTabs(),false, false);
$optionsClass = new Options();

$optionsClass->set();

$saveParams = false;
foreach (Options::getAllOptionsCode() as $code){
    if(isset($arPost[$code])){
        $saveParams = true;
        break;
    }
}

if(!empty($arPost) && $request->isPost() && $saveParams) {
    $optionsClass->saveRequest($request);
    LocalRedirect($_SERVER["SCRIPT_URL"]);
}

if($optionsClass->count() > 0) {

    $tabControl->Begin(); ?>
        <form method="POST" action="<? echo $APPLICATION->GetCurPage().$actionParams; ?>?lang=ru" ENCTYPE="multipart/form-data" name="adssform">
            <div class="adss-container">
            <?php
                foreach ($optionsClass->get() as $options){
                    $tabControl->BeginNextTab();

                    foreach ($options as $option) {

                        $class = $optionsClass->getClass($option);

                        if($class){
                            $class->getTemplate();
                        }
                    }
                    $tabControl->EndTab();
                }
            ?>
            </div>
            <?

    // завершение формы - вывод кнопок сохранения изменений
    $tabControl->Buttons(array(
        "back_url" => $_REQUEST["back_url"],
        "btnApply" => true, // не показывать кнопку применить
        "btnSave" => false,  // не показывать кнопку сохранить
    ));

    $tabControl->End();
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>

