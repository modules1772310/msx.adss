<?php
namespace Msx\Adss\Controllers;

class Tabs extends BaseController
{
    public static $tabs = [];
    public static function setTabs(){
        foreach (self::$setting as $tab){
            self::$tabs[] = $tab['TAB_CONTROLS'];
        }
    }
    public static function getTabs(){
        return self::$tabs;
    }
}