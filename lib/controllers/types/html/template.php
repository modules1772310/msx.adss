<?php
/**
 * @var array $arResult
 */
?>

<?php CUtil::InitJSCore(array('window')); ?>

<div class="item">
    <div>
        <div class="inner_wrapper ">
            <div class="title_wrapper">
                <div class="subtitle"><?=$arResult['NAME']?></div>
            </div>
            <div class="value_wrapper">
                <?
                \CFileMan::AddHTMLEditorFrame(
                    $arResult['CODE'],
                    $arResult["VALUE"],
                     $arResult['CODE']."_type",
                    'html',
                    array(
                        'height' => 150,
                        'width' => '100%'
                    ),
                    "N",
                    0,
                    "",
                    "",
                    '',
                    true,
                    false,
                    [
                        "use_editor_3" => true,
                    ],

                );
                ?>
            </div>
        </div>
    </div>
</div>

