<?php
namespace Msx\Adss\Controllers\Types\Head;

use Msx\Adss\Controllers\Options;
use Msx\Adss\Interfaces\Options as InterfaceOptions;
class Controller extends Options implements InterfaceOptions
{
    private $optData;
    public function __construct($opt)
    {
        $this->optData = $opt;
    }
    public function getTemplate()
    {
        $arResult = $this->optData;
        if(file_exists(__DIR__.'/template.php')){
            include 'template.php';
        }
    }
    /**
     * @param $request
     * @param $option
     * @return void
     */
    public function save($request, $option)
    {
    }
}