<?php
namespace Msx\Adss\Controllers\Types\File;

use Msx\Adss\Controllers\Options;
use Msx\Adss\Interfaces\Options as InterfaceOptions;
class Controller extends Options implements InterfaceOptions
{
    private $optData;
    public function __construct($opt)
    {
        $this->optData = $opt;
        $this->getData();
    }
    public function getTemplate()
    {
        $arResult = $this->optData;
        if(file_exists(__DIR__.'/template.php')){
            include 'template.php';
        }
    }
    private function getData(){
        if(!empty($data = $this->getDataBase($this->optData))){
            $this->optData['VALUE'] = $data['DATA'];
        }else{
            $this->optData['VALUE'] = [];
        }
    }

    /**
     * @param $request
     * @param $option
     * @return void
     */
    public function save($request, $option)
    {
        $arFile = $request->getFile($option['CODE']);
        $fileId = $request->getPost($option['CODE']);
        $data = [
            'CODE' => $option['CODE'],
            'TYPE' => $option['TYPE'],
        ];
        if($option['BLOCK'])$data["BLOCK"] = $option['BLOCK'];


        if(!empty($arFile['name']) || empty($fileId)) {
            $resultID = \CFile::SaveFile($arFile, "msx.adss");
            $pathFile = \CFile::GetPath($resultID);
            $data['DATA'] = [
                'SRC' => $pathFile,
                'ID' => $resultID
            ];

            if ($dataBase = $this->getDataBase($option)) {
                if(!empty($data['DATA']['ID'])){
                    $this->update($dataBase['ID'], $data);
                }elseif(empty($fileId)){
                    $data['DATA'] = [];
                    $this->update($dataBase['ID'], $data);
                }
            } else {
                $this->add($data);
            }
        }
    }
}
?>
<script>
     window.deleteFile = (code) => {
         code.value='';
        let srclink = document.getElementById(code.name+'_src');
        srclink.parentNode.remove();

    }

</script>
