<?/**
 * @var array $arResult
 */?>

<div class="item">
    <div>
        <div class="inner_wrapper">
            <div class="title_wrapper">
                <div class="subtitle"><?=$arResult['NAME']?></div>
            </div>
            <div class="value_wrapper">
                <div class="adm-input-file-control">
                    <span class="adm-input-file-exist-cont">
                        <div class="adm-input-file-ex-wrap">
                            <?if($arResult["VALUE"]):?>
                                <div class="flex-col">
                                    <input value="<?=$arResult["VALUE"]['ID']?>" type="hidden" name="<?=$arResult['CODE']?>" id="<?=$arResult['CODE']?>_src">
                                    <a title="Увеличить" class="link <?=$arResult['CODE']?>" id="<?=$arResult['CODE']?>_src" href="<?=$arResult["VALUE"]['SRC']?>" download=""><?=$arResult["VALUE"]['SRC']?></a>
                                    <span class="bx-core-popup-menu-item" title="" onclick="deleteFile(<?=$arResult['CODE']?>)">
                                        <span class="bx-core-popup-menu-item-icon adm-menu-delete"></span>
                                        <span class="bx-core-popup-menu-item-text">Удалить</span>
                                    </span>
                                </div>
                            <?endif;?>
                        </div>
                        <div class="flex-col">

                            <input
                                accept=".pdf,.docx,.txt"
                                type="file"
                                name="<?=$arResult['CODE']?>"
                                <?if($arResult["VALUE"]):?> value="<?=$arResult["VALUE"]['SRC']?>" <?endif;?>
                                id="<?=$arResult['CODE']?>"
                                class="adm-designed-file"
                            >
                        </div>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .flex-col{
        display: flex;
        align-items: center;
    }
    .adss-container .item > div {
        border: 1px solid #dce7ed;
        border-radius: 3px;
        padding: 19px 24px 20px;
        margin: -1px 0px -1px;
    }
    .adss-container .subtitle {
        margin: 0px 0px 7px;
        font-size: 13px;
        color: #000;
    }
    .adss-container .item > div .inner_wrapper.checkbox .title_wrapper {
        float: right;
        margin: 0px 0px 0px 25px;
    }
    .adss-container .item > div .inner_wrapper.checkbox .value_wrapper {
        float: left;
        position: absolute;
        top: 0px;
        left: 0px;
    }
    #bx-admin-prefix .adm-input-file-exist-cont img{
        border: none;
    }
</style>