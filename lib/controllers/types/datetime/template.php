<div class="block">
    <div class="item js_block text">
        <div>
            <div class="inner_wrapper">
                <div class="title_wrapper">
                    <div class="subtitle"><?=$arResult['NAME']?></div>
                </div>
                <div class="value_wrapper">
                    <input type="datetime-local" size="<?=$arResult['SIZE']?>" value="<?=$arResult['VALUE']?>" name="<?=$arResult['CODE']?>">
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .adss-container .item > div {
        border: 1px solid #dce7ed;
        border-radius: 3px;
        padding: 19px 24px 20px;
        margin: -1px 0px -1px;
    }
    .adss-container .subtitle {
        margin: 0px 0px 7px;
        font-size: 13px;
        color: #000;
    }
    .adss-container input[type="text"] {
        height: 33px;
        box-shadow: none;
        border-color: #c1d2db;
        padding-left: 8px;
        padding-right: 8px;
        /*width: 100%;*/
        min-width: 222px;
    }
</style>