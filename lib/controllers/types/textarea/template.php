
<div class="item">
    <div>
        <div class="inner_wrapper ">

            <div class="title_wrapperadm-detail-valign-top">
                <div class="subtitle"><?=$arResult['NAME']?></div>
            </div>
            <div class="value_wrapper">
                <textarea rows="<?=$arResult['ROWS']?>" cols="<?=$arResult['COLS']?>" name="<?=$arResult['CODE']?>"><?=$arResult['VALUE']?></textarea>
            </div>
        </div>
    </div>
</div>

<style>
    .adss-container .item > div {
        border: 1px solid #dce7ed;
        border-radius: 3px;
        padding: 19px 24px 20px;
        margin: -1px 0px -1px;
    }
    .adss-container .subtitle {
        margin: 0px 0px 7px;
        font-size: 13px;
        color: #000;
    }
    .adss-container textarea {
        resize: none;
        width: 100%;
        padding: 5px;
        border: 1px solid;
        border-color: #87919c #959ea9 #9ea7b1 #959ea9;
        border-radius: 3px;
        background: #fff;
        box-shadow: 0 1px 0 0 rgba(255, 255, 255, 0.3), inset 0 2px 2px -1px rgba(180, 188, 191, 0.7);
        color: #3f4b54;
        font-size: 13px;
    }
</style>