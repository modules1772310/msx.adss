<?php
namespace Msx\Adss\Controllers\Types\Textarea;

use Msx\Adss\Controllers\Options;
use Msx\Adss\Interfaces\Options as InterfaceOptions;
class Controller extends Options implements InterfaceOptions
{
    private $optData;
    public function __construct($opt)
    {
        $this->optData = $opt;
        $this->getData();
    }
    public function getTemplate()
    {
        $arResult = $this->optData;
        if(file_exists(__DIR__.'/template.php')){
            include 'template.php';
        }
    }
    private function getData(){
        if(!empty($data = $this->getDataBase($this->optData))){
            $this->optData['VALUE'] = $data['DATA'];
        }else{
            $this->optData['VALUE'] = $this->optData['DEFAULT_VALUE'];
        }
    }

    /**
     * @param $request
     * @param $option
     * @return void
     */
    public function save($request, $option)
    {
        $arPost = $request->getPostList()->toArray();

        $data = [
            'DATA' => $arPost[$option['CODE']],
            'CODE' => $option['CODE'],
            'TYPE' => $option['TYPE'],
        ];
        if($option['BLOCK'])$data["BLOCK"] = $option['BLOCK'];
        if(strlen($data['DATA']) == 0) $data['DATA'] = '';

        if($dataBase = $this->getDataBase($option)){
            $this->update($dataBase['ID'],$data);
        }else{
            $this->add($data);

        }
    }
}