
<div class="block_wrapper">
    <div class="item js_block selectbox">
        <div>
            <div class="inner_wrapper ">

                <div class="title_wrapper">
                    <div class="subtitle"><?=$arResult['NAME']?></div>
                </div>

                <div class="value_wrapper">
                    <select multiple size name="<?=$arResult['CODE']?>[]">
                        <?foreach ($arResult['OPTIONS'] as $key => $value){
                            if(is_array($arResult['VALUE'])){
                                if(in_array($key,$arResult['VALUE'])){?>
                                    <option value="<?=$key?>" selected><?=$value?></option>
                                <?}
                            }
                            if($key == $arResult['VALUE']){?>
                                <option value="<?=$key?>" selected><?=$value?></option>
                            <?}else{?>
                                <option value="<?=$key?>"><?=$value?></option>
                            <?}}?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .adss-container .item > div {
        border: 1px solid #dce7ed;
        border-radius: 3px;
        padding: 19px 24px 20px;
        margin: -1px 0px -1px;
    }
    .adss-container .subtitle {
        margin: 0px 0px 7px;
        font-size: 13px;
        color: #000;
    }
    .adss-container .value_wrapper select {
        height: 33px;
        box-shadow: none;
        border-color: #c1d2db;
        width: 100%;
        max-width: 222px;
    }
    .adss-container select[multiple], .adm-workarea select[size] {
        min-height: 27px;
        height: auto;
    }
</style>