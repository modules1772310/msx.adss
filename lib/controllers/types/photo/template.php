<div class="item" >
    <div>
        <div class="inner_wrapper">
            <div class="title_wrapper">
                <div class="subtitle"><?=$arResult['NAME']?></div>
            </div>
            <div class="value_wrapper">
                <div class="adm-input-file-control">
                    <span class="adm-input-file-exist-cont">
                        <div class="adm-input-file-ex-wrap">
                            <span class="adm-input-file-preview adm-input-file-bordered" style="min-width: 120px; min-height:100px; padding: 10px">
                                <a title="Увеличить" class="link <?=$arResult['CODE']?>" onclick="ImgShw('<?=$arResult["VALUE"]['SRC']?>','300','300', ''); return false;" href="<?=$arResult["VALUE"]['SRC']?>" target="_blank" >
                                    <?if(!empty($arResult["VALUE"]['SRC'])){?>
                                        <img class="image <?=$arResult['CODE']?>" src="<?=$arResult["VALUE"]['SRC']?>" alt="" width="100" height="100">
                                    <?}?>
                                </a>
                            </span>
                        </div>
                        <div class="flex-col">
                            <input value="<?=$arResult["VALUE"]['SRC']?>" type="hidden" name="<?=$arResult['CODE']?>" id="<?=$arResult['CODE']?>_src">
                            <input accept="image/*" type="file" name="<?=$arResult['CODE']?>" id="<?=$arResult['CODE']?>" class="adm-designed-file" >
                            <span class="bx-core-popup-menu-item" title="" onclick="deleteImage(<?=$arResult['CODE']?>)">
                                <span class="bx-core-popup-menu-item-icon adm-menu-delete"></span>
                                <span class="bx-core-popup-menu-item-text">Удалить</span>
                            </span>
                        </div>
                    </span>

                </div>

            </div>
        </div>
    </div>
</div>
<style>
    .flex-col{
        display: flex;
        align-items: center;
    }
    .adss-container .item > div {
        border: 1px solid #dce7ed;
        border-radius: 3px;
        padding: 19px 24px 20px;
        margin: -1px 0px -1px;
    }
    .adss-container .subtitle {
        margin: 0px 0px 7px;
        font-size: 13px;
        color: #000;
    }
    .adss-container .item > div .inner_wrapper.checkbox .title_wrapper {
        float: right;
        margin: 0px 0px 0px 25px;
    }
    .adss-container .item > div .inner_wrapper.checkbox .value_wrapper {
        float: left;
        position: absolute;
        top: 0px;
        left: 0px;
    }
    #bx-admin-prefix .adm-input-file-exist-cont img{
        border: none;
    }
</style>