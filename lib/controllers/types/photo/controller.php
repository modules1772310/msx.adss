<?php
namespace Msx\Adss\Controllers\Types\Photo;

use Msx\Adss\Controllers\Options;
use Msx\Adss\Interfaces\Options as InterfaceOptions;
class Controller extends Options implements InterfaceOptions
{
    private $optData;
    public function __construct($opt)
    {
        $this->optData = $opt;
        $this->getData();
    }
    public function getTemplate()
    {
        $arResult = $this->optData;
        if(file_exists(__DIR__.'/template.php')){
            include 'template.php';
        }
    }
    private function getData(){
        if(!empty($data = $this->getDataBase($this->optData))){
            $this->optData['VALUE'] = $data['DATA'];
        }else{
            $this->optData['VALUE'] = $this->optData['DEFAULT_VALUE'];
        }
    }

    /**
     * @param $request
     * @param $option
     * @return void
     */
    public function save($request, $option)
    {
        $arFile = $request->getFile($option['CODE']);
        $path = $request->getPost($option['CODE']);
        $data = [
            'CODE' => $option['CODE'],
            'TYPE' => $option['TYPE'],
        ];
        if($option['BLOCK'])$data["BLOCK"] = $option['BLOCK'];

        if(!empty($arFile['name']) || empty($path)) {
            $resultID = \CFile::SaveFile($arFile, "msx.adss");
            $pathFile = \CFile::GetPath($resultID);
            $data['DATA'] = [
                'SRC' => $pathFile,
                'ID' => $resultID
            ];
            if(empty($data['DATA']['ID'])) return;
            if ($dataBase = $this->getDataBase($option)) {
                $this->update($dataBase['ID'], $data);
            } else {
                $this->add($data);
            }
        }
    }
}
?>
<script>
    function deleteImage(code){
        code.value='';
        let srclink = document.getElementById(code.name+'_src');
        let link = document.getElementsByClassName('link '+code.name)[0];
        let image = document.getElementsByClassName('image '+code.name)[0];
        srclink.value = '';
        link.removeAttribute('href');
        image.removeAttribute('src')
    }
</script>
