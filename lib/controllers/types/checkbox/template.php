<div class="item">
    <div>
        <div class="inner_wrapper checkbox">
            <div class="title_wrapper">
                <div class="subtitle">
                    <label for="<?=$arResult['CODE']?>"><?=$arResult['NAME']?></label>
                </div>
            </div>
            <div class="value_wrapper">
                <input type="checkbox" id="<?=$arResult['CODE']?>" name="<?=$arResult['CODE']?>" value="Y" <?if($arResult['VALUE']== 'Y'){?>checked<?}?>  class="adm-designed-checkbox">
                <label class="adm-designed-checkbox-label" for="<?=$arResult['CODE']?>" title=""></label>
            </div>
        </div>
    </div>
</div>

<style>
    .adss-container .item > div {
        border: 1px solid #dce7ed;
        border-radius: 3px;
        padding: 19px 24px 20px;
        margin: -1px 0px -1px;
    }
    .adss-container .subtitle {
        margin: 0px 0px 7px;
        font-size: 13px;
        color: #000;
    }
    .adss-container .item > div .inner_wrapper.checkbox {
        display: inline-block;
        position: relative;
        margin: 2px 0px -11px;
        cursor: pointer;
    }
    .adss-container .item > div .inner_wrapper.checkbox .title_wrapper {
        float: right;
        margin: 0px 0px 0px 25px;
    }
    .adss-container .item > div .inner_wrapper.checkbox .value_wrapper {
        float: left;
        position: absolute;
        top: 0px;
        left: 0px;
    }
</style>