<?php

namespace Msx\Adss\Controllers;
use Msx\Adss\Controllers\Tabs;

class BaseController
{
    public static $setting = [];
    public static function setSetting($setting){
        if($setting){
            self::$setting = $setting;
        }
    }
    public static function init(){
        if(file_exists(self::getPathFile())){
            self::initFile(self::getPathFile());
        }
    }
    private static function getPathFile(){
        return $_SERVER['DOCUMENT_ROOT'].'/local/modules/msx.adss/adss.setting.php';
    }
    private static function initFile($path){
        include $path;
    }
}