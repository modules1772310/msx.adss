<?php

namespace Msx\Adss\Controllers;
use Msx\Adss\Controllers\Types;
use Msx\Adss\Model\AdssTable;
class Options extends BaseController
{

    public $options;
    private $nameClass;
    private $keyTypeArray = [];
    private $moduleID = "msx.adss";
    public function set(){
        foreach (self::$setting as $options){
            $this->options[] = $options['OPTIONS'];
        }
        return $this;
    }
    public function count(){
        if(!empty($this->get()) && is_array($this->get())){
            return count($this->get());
        }
        return false;
    }
    public function get(){
        return $this->options;
    }
    public function getCode(){
        foreach ($this->get() as $options){
            foreach ($options as $option){
                if(!empty($option['CODE'])) {
                    $this->keyTypeArray[$option['CODE']] = $option;
                }
            }
        }
        return $this->keyTypeArray;
    }
    public function saveRequest($request){
        foreach ($this->getCode() as $option){
            $class = $this->getClass($option);
             $class->save($request,$option);
        }
    }

    public function getClass($opt){
        if(!empty($opt['TYPE'])){
            $this->nameClass = $opt['TYPE'];
            $className = "\Msx\Adss\Controllers\Types\\".$this->nameClass."\Controller";
            
            if(class_exists($className)){
                return new $className($opt);
            }else{
                return false;
            }
        }
        return false;

    }
    public function getDataBase($data){
        $dbItems = AdssTable::getList(
            [
                'select' => ['DATA','ID'],
                'filter' => [
                    '=TYPE' => $data['TYPE'],
                    '=CODE' => $data['CODE']
                ],
                'cache' => array(
                    'ttl' => 3600,
                    'cache_joins' => true,
                )
            ]
        );
        return $dbItems->fetch();
    }
    public static function getOption($type,$code){
        $dbItems = AdssTable::getList(
            [
                'select' => ['*'],
                'filter' => [
                    '=TYPE' => $type,
                    '=CODE' => $code
                ],
                'cache' => array(
                    'ttl' => 3600,
                    'cache_joins' => true,
                )
            ]
        );
        return $dbItems->fetch()['DATA'];
    }

    public static function getOptionsByBlock($blockName){
        $dbItems = AdssTable::getList(
            [
                'select' => ['*'],
                'filter' => [
                    '=BLOCK' => $blockName,
                ],
                'cache' => array(
                    'ttl' => 3600,
                    'cache_joins' => true,
                )
            ]
        );
        $result = [];
        while($arFields = $dbItems->fetch()){
            $result[$arFields["CODE"]] = $arFields['DATA'];
        }
        return $result;
    }

    public static function getAllOptionsCode(){
        $dbItems = AdssTable::getList(
            [
                'select' => ['CODE'],
                'cache' => array(
                    'ttl' => 3600,
                    'cache_joins' => true,
                )
            ]
        );
        $result = [];
        while($arFields = $dbItems->fetch()){
            $result[] = $arFields["CODE"];
        }

        if(empty($result) && !empty(self::$setting)){
            foreach (self::$setting as $tab){
               foreach ( $tab["OPTIONS"] as $option){
                   if($option["CODE"]) $result[] = $option["CODE"];
               }
            }
        }

        return $result;
    }


    public function update($id,$data){
        AdssTable::update($id,$data);
    }
    public function add($data){
        AdssTable::Add($data);
    }
}