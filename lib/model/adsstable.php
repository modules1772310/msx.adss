<?php

namespace Msx\Adss\Model;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Fields\Validators\LengthValidator;
use Bitrix\Main\ORM\Fields\Validators\UniqueValidator;

class AdssTable extends Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'msx_adss_setting';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new Entity\IntegerField('ID', [
                'autocomplete' => true,
                'primary' => true,
            ]),
            new Entity\TextField('DATA', [
                'required' => false,
                'primary' =>false,
                'save_data_modification' => function () {
                    return array(
                        function ($value) {
                            return serialize($value);
                        }
                    );
                },
                'fetch_data_modification' => function () {
                    return array(
                        function ($value) {
                            return unserialize($value);
                        }
                    );
                }
            ]),
            new Entity\StringField('TYPE', [
                'validation'=> array(__CLASS__, 'validateString'),
            ]),
            new Entity\StringField('CODE', [
                'validation'=> array(__CLASS__, 'validateString'),
            ]),
            new Entity\StringField('BLOCK', [
                'required' => false,
                'default_value' => '',
                'validation'=> array(__CLASS__, 'validateString'),
            ]),
            new Entity\StringField('MODULE', [
                'default_value' => 'msx.adss'
            ]),
            new Entity\DatetimeField('DATE_ADD', [
                'default_value' => function () {
                    return new \Bitrix\Main\Type\Date(new \Bitrix\Main\Type\DateTime());
                }
            ]),
        ];
    }

    public static function truncateTable()
    {
        $connection = \Bitrix\Main\Application::getConnection();
        return $connection->truncateTable(self::getTableName());
    }

    public static function dropTable()
    {
        $connection = \Bitrix\Main\Application::getConnection();
        $connection->dropTable(self::getTableName());
    }

    public static function validateString()
    {
        return [
            new LengthValidator(null, 255)
        ];
    }
}