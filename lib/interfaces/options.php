<?php
namespace Msx\Adss\Interfaces;
interface Options
{
    public function getTemplate();
    public function save($request,$option);
}