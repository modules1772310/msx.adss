<?php

namespace Msx\Adss\Helper;

use Bitrix\Main\Localization\Loc;
class Menu
{
    public static function OnBuildGlobalMenuHandler(
        &$arGlobalMenu,
        &$arModuleMenu
    )
    {

        if (!isset($arGlobalMenu['global_menu_msx'])) {
            $arGlobalMenu['global_menu_msx'] = [
                'menu_id' => 'msx',
                'text' => 'Medialine',
                'title' => Loc::getMessage('TITLE'),
                'sort' => 99999,
                'items_id' => 'global_menu_msx_items',
                "icon" => "",
                "page_icon" => "",
            ];
        }
        $aMenu = array(
            "parent_menu" => 'global_menu_msx',
            "section" => 'msx.adss',
            "sort" => 200,
            "text" => Loc::getMessage('SETTING_ADSS'),
            "title" => Loc::getMessage('SETTING_ADSS'),
            "icon" => "sys_menu_icon",
            "page_icon" => "sys_menu_icon",
            'more_url' => array(
                "ml.adss.setting.php",
            ),
            "items_id" => "menu_msx.adss",
            'items' => array(
                array(
                    "icon" => "fav_menu_icon_yellow",
                    "page_icon" => "fav_menu_icon_yellow",
                    "text" => Loc::getMessage('SETTING_ADSS_SITE'),
                    "url" => "ml.adss.setting.php?lang=" . LANGUAGE_ID,
                    "more_url" => array(),
                    "title" =>Loc::getMessage('SETTING_ADSS_SITE'),
                ),
            )
        );
        $arGlobalMenu['global_menu_msx']['items']['msx.adss'] = $aMenu;
    }
}