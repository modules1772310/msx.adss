<?php
function setSetting($setting){
    \Msx\Adss\Controllers\BaseController::setSetting($setting);
}
function getOptionAdss($type,$code){
    return \Msx\Adss\Controllers\Options::getOption($type,$code);
}
function getOptionsAdssByBlock($blockName){
    return \Msx\Adss\Controllers\Options::getOptionsByBlock($blockName);
}