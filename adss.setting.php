<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Msx\Adss\Model\AdssTable;

Loc::loadMessages(__FILE__);

$arAllOptions = [
    'tab1' => [
        'TAB_CONTROLS' => [

            "DIV" => "edit1",
            "TAB" => 'Общие настройки',
            "ICON" => "main_settings",
            "TITLE" => 'Общие настройки',
        ],
        'OPTIONS' => [
            [
                "BLOCK" =>'HEADER',
                "TYPE" => 'photo',
                "NAME" => 'Лого',
                "CODE" => "LOGO",
            ],
            [
                "TYPE" => 'head',
                "NAME" => 'Header',
            ],
			[
                "BLOCK" =>'HEADER',
                "TYPE" => 'text',
                "NAME" => 'Телефон 1',
                "DEFAULT_VALUE" => '',
                "CODE" => "HEADER_PHONE_1",
                "SIZE" => '60'
            ],
            [
                "BLOCK" =>'HEADER',
                "TYPE" => 'text',
                "NAME" => 'Телефон 2',
                "DEFAULT_VALUE" => '',
                "CODE" => "HEADER_PHONE_2",
                "SIZE" => '60'
            ],
            [
                "TYPE" => 'head',
                "NAME" => 'Footer',
            ],
            [
                "BLOCK" =>'FOOTER',
                "TYPE" => 'text',
                "NAME" => 'Телефон 1',
                "DEFAULT_VALUE" => '',
                "CODE" => "FOOTER_PHONE_1",
                "SIZE" => '60'
            ],
            [
                "BLOCK" =>'FOOTER',
                "TYPE" => 'text',
                "NAME" => 'Телефон 2',
                "DEFAULT_VALUE" => '',
                "CODE" => "FOOTER_PHONE_2",
                "SIZE" => '60'
            ],
            [
                "BLOCK" =>'FOOTER',
                "TYPE" => 'text',
                "NAME" => 'Почта',
                "DEFAULT_VALUE" => '',
                "CODE" => "FOOTER_EMAIL",
                "SIZE" => '60'
            ],[
                "BLOCK" =>'FOOTER',
                "TYPE" => 'text',
                "NAME" => 'Текст под футером',
                "DEFAULT_VALUE" => '',
                "CODE" => "FOOTER_UNDER_TEXT",
                "SIZE" => '80'
            ],
            [
                "TYPE" => 'head',
                "NAME" => 'В разработке',
            ],
            [
                "BLOCK" =>'IN_DEVELOPMENT',
                "TYPE" => 'textarea',
                "NAME" => 'Заголовок блока в разработке',
                "DEFAULT_VALUE" => '',
                "CODE" => "IN_DEVELOPMENT_TITLE",
            ],
            [
                "BLOCK" =>'IN_DEVELOPMENT',
                "TYPE" => 'textarea',
                "NAME" => 'Содержимое блока в разработке',
                "DEFAULT_VALUE" => '',
                "CODE" => "IN_DEVELOPMENT_BODY",
                "ROWS" => 10,
            ],
        ]

    ],
    'tab2' => [
        'TAB_CONTROLS' => [
            "DIV" => "edit2",
            "TAB" => 'Ссылки на соц. сети',
            "ICON" => "main_settings",
            "TITLE" => 'Соц.сеть',
        ],
        'OPTIONS' => [
            [
                "BLOCK" => "HEADER",
                "TYPE" => 'text',
                "NAME" => 'Ссылка на соц сеть YouTube',
                "DEFAULT_VALUE" => '',
                "CODE" => "HREF_YOUTUBE",
                "SIZE" => '60'
            ],
            [
                "BLOCK" => "HEADER",
                "TYPE" => 'text',
                "NAME" => 'Ссылка на соц сеть Telegram',
                "DEFAULT_VALUE" => 'https://telegram.me/share/url?',
                "CODE" => "HREF_TELEGRAM",
                "SIZE" => '60'
            ],
        ]
    ],
];
setSetting($arAllOptions);
