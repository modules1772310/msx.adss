<?

use Msx\Adss\Model\AdssTable;
use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\ModuleManager,
    \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Bitrix\Main\EventManager,
    \Bitrix\Main\IO\Directory;

Loc::loadMessages(__FILE__);

class msx_adss extends CModule
{
	public function __construct(){

        if(file_exists(__DIR__."/version.php")){

            $arModuleVersion = array();

            include_once(__DIR__."/version.php");

            $this->MODULE_ID            = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION       = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME          = Loc::getMessage("ML_ADSS_NAME");
            $this->MODULE_DESCRIPTION  = Loc::getMessage("ML_ADSS_DESCRIPTION");
            $this->PARTNER_NAME     = Loc::getMessage("ML_ADSS_PARTNER_NAME");
            $this->PARTNER_URI      = Loc::getMessage("ML_ADSS_PARTNER_URI");
            $this->MODULE_GROUP_RIGHTS = 'Y';
            $this->MODULE_SORT = 1;
        }

        return false;
    }	
	public function DoInstall()
	{
		global $APPLICATION;
        if ($this->isVersionD7()) {
            ModuleManager::registerModule($this->MODULE_ID);            
        }else{
            RegisterModule($this->MODULE_ID);
        }
        $this->InstallEvents();
        $this->InstallDB();
        $this->InstallFiles();
		return true;
	}

	public function DoUninstall()
	{
        if ($this->isVersionD7()) {
            ModuleManager::unRegisterModule($this->MODULE_ID);
        }else{
            UnRegisterModule($this->MODULE_ID);
        }
        $this->UnInstallEvents();
        $this->UnInstallDB();
        $this->UnInstallFiles();
		return true;
	}
    
    public function GetModuleRightList()
    {
        return [
                    "reference_id" => ["D", "R", "W"],
                    "reference" => [
                        "[D] ".Loc::getMessage("ML_ADSS_DENIED"),
                        "[R] ".Loc::getMessage("ML_ADSS_READ"),
                        "[W] ".Loc::getMessage("ML_ADSS_WRITE")
                    ]
        ];
    }

    public function InstallDB()
    {
        if(Loader::includeModule($this->MODULE_ID)) {
            foreach ($this->getTableList() as $class) {
                if (!$class::getEntity()->getConnection()->isTableExists($class::getTableName())) {
                    $class::getEntity()->createDbTable();
                }
            }
        }
        return true;
    }

    public function UnInstallDB()
    {
        if(Loader::includeModule($this->MODULE_ID)) {
            foreach ($this->getTableList() as $class) {
                if ($class::getEntity()->getConnection()->isTableExists($class::getTableName())) {
                    //$class::dropTable();
                }
            }
        }
        return true;
    }

    public function InstallEvents()
    {
        $eventManager = EventManager::getInstance();

        foreach ($this->getEventsList() as $event) {
            $eventManager->registerEventHandler(
                $event['module'],
                $event['event'],
                $this->MODULE_ID,
                $event['class'],
                $event['method']
            );
        }
        $eventManager->registerEventHandlerCompatible("main", "OnBeforeProlog", "msx.adss");

        return true;
    }
    public function UnInstallEvents()
    {
        $eventManager = EventManager::getInstance();

        foreach ($this->getEventsList() as $event) {
            $eventManager->unRegisterEventHandler(
                $event['module'],
                $event['event'],
                $this->MODULE_ID,
                $event['class'],
                $event['method']
            );
        }
        $eventManager->unRegisterEventHandler("main", "OnBeforeProlog", "msx.adss");
        return true;
    }
    private function getTableList(){
        return [
            AdssTable::class,
        ];
    }
    private function getEventsList()
    {
        return [
            [
                'module' => 'main',
                'event' => 'OnBuildGlobalMenu',
                'class' => 'Msx\Adss\Helper\Menu',
                'method' => 'OnBuildGlobalMenuHandler',
            ],
        ];
    }

    public function InstallFiles(){
        CopyDirFiles(__DIR__ .'/admin', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin', true, true);
        CopyDirFiles(__DIR__ . '/themes/.default', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/themes/.default/', true, true);
        return true;
    }

    public function UnInstallFiles(){
        DeleteDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/admin', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin');
        DeleteDirFilesEx('/bitrix/themes/.default/icons/' . $this->MODULE_ID);
        DeleteDirFilesEx('/bitrix/themes/.default/' . $this->MODULE_ID . '.css');
        return true;
    }
    
    public function isVersionD7()
    {
        return CheckVersion(ModuleManager::getVersion('main'), '21.00.00');
    }
}
?>