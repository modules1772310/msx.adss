<?
/***
 * Данный пример демонстрирует один из способов написания файла options для модулей Битрикса.
 *
 * Приведённый пример расчитан на модуль с однотипными свойствами, число которых чётко фиксировано.
 * Список свойств вынесен в отдельную переменную $arOptions, таким образом добавление нового свойства - вопрос 1 минуты
 */
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();

$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);

Loader::includeModule($module_id);

$arAllOptions = [
    'tab1' => [
        'TAB_CONTROLS' => [
                "DIV" => "edit1", 
                "TAB" => Loc::getMessage("ML_MODULE_NAME_TABS_NAME"),
                "ICON" => "main_settings",
                "TITLE" => Loc::getMessage("ML_MODULE_NAME_TABS_TITLE")
        ],
        'OPTIONS' => [
            [
                "name_string",
                Loc::getMessage("ML_MODULE_NAME_TEXT"),
                Loc::getMessage("ML_MODULE_NAME_DEFAULT"),
                ["text", 90]
            ],
            [
                "name_password",
                Loc::getMessage("ML_MODULE_NAME_PASSWORD"),
                "password",
                ["password", 90]
            ],
            [
                "name_checkbox",
                Loc::getMessage("ML_MODULE_NAME_CHECKBOX"),
                "Y",
                [
                    "checkbox",
                    "",
                    "disabled"
                ]
            ],
            [
                "name_datetime",
                Loc::getMessage("ML_MODULE_NAME_TEXT"),
                Loc::getMessage("ML_MODULE_NAME_DEFAULT"),
                ["datetime", 90]
            ],
            [
                "name_option_select",
                Loc::getMessage("ML_MODULE_NAME_SELECT"),
                "option1",
                ["selectbox", [
                    'option1'=> Loc::getMessage("ML_MODULE_NAME_OPTION") . ' 1',
                    'option2'=> Loc::getMessage("ML_MODULE_NAME_OPTION") . ' 2',
                    'option3'=> Loc::getMessage("ML_MODULE_NAME_OPTION") . ' 3'
                ]],
            ],
            [
                "multiselect_name",
                Loc::getMessage("ML_MODULE_NAME_MULTISELECT"),
                "option2,option3",
                array(
                    "multiselectbox",
                    array(
                        'option1'=> Loc::getMessage("ML_MODULE_NAME_OPTION") . ' 1',
                        'option2'=> Loc::getMessage("ML_MODULE_NAME_OPTION") . ' 2',
                        'option3'=> Loc::getMessage("ML_MODULE_NAME_OPTION") . ' 3'
                    )
                )
            ],
            [
                "textarea_name",
                Loc::getMessage("ML_MODULE_NAME_MULTYTEXT"),
                Loc::getMessage("ML_MODULE_NAME_DEFAULT"),
                [
                    "textarea",
                    "12", // rows
                    "60", // cols
                ]
            ],
            Loc::getMessage("ML_MODULE_NAME_TITLE"),            
            [
                "note" => Loc::getMessage("ML_MODULE_NAME_NOTE")
            ],
            [
                "static_text_name",
                Loc::getMessage("ML_MODULE_NAME_STATIC_TEXT"),
                Loc::getMessage("ML_MODULE_NAME_DEFAULT"),
                array(
                    "statictext"
                )
            ],
            [
                "static_html_name",
                Loc::getMessage("ML_MODULE_NAME_STATIC_TEXT") . " html",
                Loc::getMessage("ML_MODULE_NAME_DEFAULT") . "<br>" . Loc::getMessage("ML_MODULE_NAME_DEFAULT"),
                array(
                    "statictext"
                )
            ],                        
        ]               

    ],
    'tab2' => [
        'TAB_CONTROLS' => [
                "DIV" => "edit2", 
                "TAB" => Loc::getMessage("ML_MODULE_NAME_TABS_NAME"),
                "ICON" => "main_settings",
                "TITLE" => Loc::getMessage("ML_MODULE_NAME_TABS_TITLE")
        ],
        'OPTIONS' => [
            Loc::getMessage("ML_MODULE_NAME_TITLE"),            
            [
                "note" => Loc::getMessage("ML_MODULE_NAME_NOTE")
            ],
            [
                "static_text_name_tab2",
                Loc::getMessage("ML_MODULE_NAME_STATIC_TEXT"),
                Loc::getMessage("ML_MODULE_NAME_DEFAULT"),
                array(
                    "statictext"
                )
            ]                      
        ]               

    ],

];
foreach($arAllOptions as $arTab){
    $aTabs[] = $arTab['TAB_CONTROLS'];
}
if($request->isPost() && check_bitrix_sessid())
{
    foreach($arAllOptions as $aOptGroup)
    {
        foreach($aOptGroup['OPTIONS'] as $option)
        {
            __AdmSettingsSaveOption($module_id, $option);
        }
    }
    LocalRedirect($APPLICATION->GetCurPage()."?mid=".$module_id."&lang=".LANG);
}

$tabControl = new CAdminTabControl("tabControl", $aTabs);

function ShowParamsHTMLByArray($arParams, $module_id)
{
    foreach($arParams as $Option)
    {
        __AdmSettingsDrawRow($module_id, $Option);
    }
}
?>
<form action="<? echo($APPLICATION->GetCurPage()); ?>?mid=<? echo($module_id); ?>&lang=<? echo(LANG); ?>" method="post">
    <?= bitrix_sessid_post()?>
    <?
    $tabControl->Begin();
    foreach($arAllOptions as $key => $option){
        $tabControl->BeginNextTab();
        ShowParamsHTMLByArray($option['OPTIONS'], $module_id);
    }
    ?>

  <?
   $tabControl->Buttons();
  ?>
    <input type="submit" name="apply" value="<?=Loc::GetMessage("ML_MODULE_NAME_SAVE");?>" class="adm-btn-save" />
    <?$tabControl->End();?>
</form>

<!--
use Msx\Adss\Controllers\Options as MlOptions;
$options = MlOptions::getOptionsByBlock('LOT');
-->